import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  var formKey = GlobalKey<FormState>();
  var nameCtrl = TextEditingController();
  var phoneCtrl = TextEditingController();
  var addressCtrl = TextEditingController();
  var name, phone, address, gender;

  List users = [];
  _loadData() async {
    var jsonString = await rootBundle.loadString('asset/data.json');
    setState(() {
      this.users = json.decode(jsonString);
    });
  }

  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form Validation'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextFormField(
                    controller: nameCtrl,
                    decoration: InputDecoration(labelText: 'Enter Name'),
                    validator: (value) {
                      if (value.length == 0) return ("Name is required!");
                    },
                    onSaved: (value) {
                      this.name = value;
                    },
                  ),
                  DropdownButtonFormField(
                    validator: (val) {
                      // do validation
                    },
                    onChanged: (val) {
                      setState(() {
                        this.gender = val;
                      });
                    },
                    value: this.gender,
                    hint: Text('Please select gender'),
                    items: [
                      DropdownMenuItem(
                        value: 'male',
                        child: Text('Male'),
                      ),
                      DropdownMenuItem(
                        value: 'female',
                        child: Text('Female'),
                      ),
                    ],
                  ),
                  DropdownButtonFormField(
                      // phone dropdown
                      validator: (val) {
                        // do validation
                      },
                      onChanged: (val) {
                        setState(() {
                          this.phone = val;
                        });
                      },
                      value: this.phone,
                      hint: Text('Please select Phone Number'),
                      items: users.map((user) {
                        return DropdownMenuItem<String>(
                          value: user['phone'],
                          child: Text(user['phone']),
                        );
                      }).toList()),
                  TextFormField(
                    controller: addressCtrl,
                    decoration: InputDecoration(labelText: 'Enter Address'),
                    validator: (value) {
                      if (value.length == 0) return ("Address is required!");
                    },
                    onSaved: (value) {
                      this.address = value;
                    },
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          child: Text('Submit'), onPressed: submitHandle),
                      SizedBox(
                        width: 10.0,
                      ),
                      ElevatedButton(
                        child: Text('Reset'),
                        onPressed: resetHandle,
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void submitHandle() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      print("Name is: ${this.name}");
    }
  }

  void resetHandle() {
    nameCtrl.clear();
    phoneCtrl.clear();
    addressCtrl.clear();
  }
}
