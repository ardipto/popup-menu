import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:popup_menu/models/user.dart';

class PopupMenuPage extends StatefulWidget {
  @override
  _PopupMenuPageState createState() => _PopupMenuPageState();
}

class _PopupMenuPageState extends State<PopupMenuPage> {
  List users = [];
  _loadData() async {
    var jsonString = await rootBundle.loadString('asset/data.json');
    setState(() {
      this.users = json.decode(jsonString);
    });
  }

  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Popup menu'),
        actions: [
          PopupMenuButton(
            offset: Offset(0, 55),
            elevation: 10,
            onSelected: (val) {},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(
                  value: 'rating',
                  child: Text('Rating'),
                ),
                PopupMenuItem(
                  value: 'website',
                  child: Text('Website'),
                ),
              ];
            },
          )
        ],
      ),
      body: Container(
        width: double.infinity,
        child: ListView.builder(
          itemCount: users == null ? 0 : users.length,
          itemBuilder: (BuildContext context, index) {
            User user = User.fromJson(users[index]);
            return ListTile(
              leading: CircleAvatar(
                child: Text(user.firstName[0]),
              ),
              title: Text(user.firstName),
              subtitle: Text(user.email),
            );
          },
        ),
      ),
    );
  }
}
