import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:popup_menu/models/user.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncherPage extends StatefulWidget {
  @override
  _UrlLauncherPageState createState() => _UrlLauncherPageState();
}

class _UrlLauncherPageState extends State<UrlLauncherPage> {
  List users = [];
  _loadData() async {
    var jsonString = await rootBundle.loadString('asset/data.json');
    setState(() {
      this.users = json.decode(jsonString);
    });
  }

  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('URL Launcher'),
        actions: [
          PopupMenuButton(
            offset: Offset(0, 55),
            elevation: 10,
            onSelected: (val) {},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(
                  value: 'rating',
                  child: Text('Rating'),
                ),
                PopupMenuItem(
                  value: 'website',
                  child: Text('Website'),
                ),
              ];
            },
          )
        ],
      ),
      body: Container(
        width: double.infinity,
        child: ListView.builder(
          itemCount: users == null ? 0 : users.length,
          itemBuilder: (BuildContext context, index) {
            User user = User.fromJson(users[index]);
            return ListTile(
              leading: CircleAvatar(
                child: Text(user.firstName[0]),
              ),
              title: Text(user.firstName),
              subtitle: Text(user.email),
              trailing: listPopupMenuButton(user),
            );
          },
        ),
      ),
    );
  }

  listPopupMenuButton(User user) {
    return PopupMenuButton(
      offset: Offset(0, 20),
      elevation: 10,
      onSelected: (val) async {
        switch (val) {
          case 'call':
            if (await canLaunch("tel:${user.phone}")) {
              await launch("tel:${user.phone}");
            }
            break;
          case 'website':
            if (await canLaunch("http:${user.website}")) {
              await launch("http:${user.website}");
            }
            break;
          case 'sms':
            if (await canLaunch("sms:${user.phone}")) {
              await launch("sms:${user.phone}");
            }
            break;
        }
      },
      itemBuilder: (BuildContext context) {
        return [
          PopupMenuItem(
            value: 'call',
            child: Row(
              children: [Icon(Icons.phone), SizedBox(width: 10), Text('Call')],
            ),
          ),
          PopupMenuItem(
            value: 'website',
            child: Row(
              children: [
                Icon(Icons.web),
                SizedBox(width: 10),
                Text('Visit Website')
              ],
            ),
          ),
          PopupMenuItem(
            value: 'sms',
            child: Row(
              children: [Icon(Icons.sms), SizedBox(width: 10), Text('SMS')],
            ),
          ),
        ];
      },
    );
  }
}
