class User {
  int id;
  String firstName;
  String lastName;
  String email;
  String gender;
  String website;
  String phone;

  User(this.id, this.firstName, this.lastName, this.email, this.gender,
      this.website, this.phone);

  User.fromJson(dynamic json) {
    id = json["id"];
    firstName = json["first_name"];
    lastName = json["last_name"];
    email = json["email"];
    gender = json["gender"];
    website = json["website"];
    phone = json["phone"];
  }
}
